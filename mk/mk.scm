(import (rnrs) (mk-r6rs core))

(mk-r6rs '(rsc3 disassembler)
	 '("../src/graphdef.scm" "../src/disassembler.scm")
	 (string-append (list-ref (command-line) 1) "/rsc3/disassembler.sls")
	 '((rnrs) (rhs core) (sosc core) (rsc3 core) (rsc3 server) (rsc3 ugen) (rsc3 dot))
	 '()
	 '())

(exit)
