(import (rnrs) (rsc3 core) (rsc3 ugen) (rsc3 disassembler))

(define sy-dir "/home/rohan/sw/rsc3-disassembler/scsyndef/")

(define sy-name (lambda (x) (string-append sy-dir x ".scsyndef")))

(define store
  (lambda (sy)
    (synthdefWrite sy (sy-name (graphdef-name sy)))))

(store
 (synthdef
  "simple"
  (Out 0 (SinOsc 440 0))))

(store
 (synthdef
  "with-ctl"
  (letc ((freq 440))
    (Out 0 (Mul (SinOsc freq 0) 0.1)))))

(store
 (synthdef
  "mce"
  (Out 0 (Mul (SinOsc (Mce3 440 441 442) 0) 0.1))))

(store
 (synthdef
  "mce-eq"
  (Out 0 (Mul (SinOsc (Mce3 440 440 440) 0) 0.1))))

(store
 (synthdef
  "mix"
  (Out 0 (Mul (Mix (SinOsc (Mce3 440 441 442) 0)) 0.1))))

(store
 (synthdef
  "add"
  (Out 0 (Add (Mul (SinOsc 440 0) 0.1) (Mul (PinkNoise) 0.1)))))

(store
 (synthdef
  "nary-math"
  (Out
   0
   (Max
    (Max (Mul (SinOsc 440 0) 0.1) (Mul (Dust 15) 0.1))
    (Mul (PinkNoise) 0.1)))))

(store
 (synthdef
  "controls"
  (letc ((freq 440) (ampl 0.1))
    (Out 0 (MulAdd (SinOsc freq 0) ampl (Mul (PinkNoise) 0.1))))))

(store
 (synthdef
  "jmcc"
  (let* ((o (MulAdd (LfSaw (list 8 7.23) 0) 3 80))
	 (f (MulAdd (LfSaw 0.4 0) 24 o))
	 (s (Mul (SinOsc (MidiCps f) 0) 0.04)))
    (Out 0 (CombN s 0.2 0.2 4)))))

(store
 (synthdef
  "mrg"
  (Mrg2
   (Out 0 (Mul (SinOsc 440 0) 0.1))
   (Out 1 (Mul (SinOsc 441 0) 0.1)))))

(define sy-load (lambda (x) (read-graphdef-file (sy-name x))))

(define sy-disassemble (lambda (x) (graphdef-disassemble (sy-load x))))

(sy-disassemble "simple")
(sy-disassemble "with-ctl")
(sy-disassemble "mce")
(sy-disassemble "mce-eq")
(sy-disassemble "mix")
(sy-disassemble "add")
(sy-disassemble "nary-math")
(sy-disassemble "controls")
(sy-disassemble "jmcc")
(sy-disassemble "mrg") ; error
