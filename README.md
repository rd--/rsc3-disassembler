rsc3-disassembler
-----------------

[rsc3](http://rohandrape.net/?t=rsc3)
[sc3](http://audiosynth.com/)
synth graph disassembler.

The function `read-graphdef-file` will load and parse a `.scsyndef`
file into a `graphdef` value.

The function `graphdef-disassemble` will generate an s-expression from
a `graphdef` value.

The graph:

~~~~
(let* ((o (MulAdd (LfSaw (list 8 7.23) 0) 3 80))
       (f (MulAdd (LfSaw 0.4 0) 24 o))
       (s (Mul (SinOsc (MidiCps f) 0) 0.04)))
  (CombN s 0.2 0.2 4))
~~~~

is disassembled as:

~~~~
(list
 (CombN
  (Mul
   (SinOsc
    (MidiCps
     (MulAdd (LFSaw 0.4 0) 24.0 (MulAdd (LFSaw 8 0) 3 80)))
    0)
   0.4)
  0.2 0.2 4)
 (CombN
  (Mul
   (SinOsc
    (MidiCps
     (MulAdd (LFSaw 0.4 0) 24.0 (MulAdd (LFSaw 7.23 0) 3 80)))
    0)
   0.4)
  0.2 0.2 4))
~~~~

© [rohan drape](http://rohandrape.net/),
  1998-2022,
  [gpl](http://gnu.org/copyleft/)
