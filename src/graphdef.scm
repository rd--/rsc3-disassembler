; Decode a binary 'Graph Definition'.
; Files containing more than one graph definition are not supported.

(define read-control
  (lambda (p)
    (let* ((nm (read-pascal-string p))
           (ix (read-i16 p)))
      (make-control control-rate nm ix nil))))

(define read-input
  (lambda (p)
    (let* ((u (read-i16 p))
           (p (read-i16 p)))
      (make-input u p))))

(define read-output
  (lambda (p)
    (make-output
     (read-i8 p))))

(define read-ugen
  (lambda (p)
    (let* ((name (read-pascal-string p))
           (rate (read-i8 p))
           (number-of-inputs (read-i16 p))
           (number-of-outputs (read-i16 p))
           (special (read-i16 p))
           (inputs (replicateM number-of-inputs (lambda () (read-input p))))
           (outputs (replicateM number-of-outputs (lambda () (read-output p)))))
      (make-ugen name rate inputs outputs special))))

(define read-list
  (lambda (n f p)
    (replicateM n (lambda () (f p)))))

(define read-graphdef
  (lambda (p)
    (let* ((magic (read-bytevector p 4))
           (version (read-i32 p))
           (number-of-definitions (read-i16 p)))
      (if (not (bytevector=? magic (string->utf8 "SCgf")))
          (error "read-graphdef" "illegal magic string" magic))
      (if (not (= version 0))
          (error "read-graphdef" "version not at zero" version))
      (if (not (= number-of-definitions 1))
          (error "read-graphdef" "non unary graphdef file" number-of-definitions))
      (let* ((name (read-pascal-string p))
             (number-of-constants (read-i16 p))
             (constants (read-list number-of-constants read-f32 p))
             (number-of-control-defaults (read-i16 p))
             (control-defaults (read-list number-of-control-defaults read-f32 p))
             (number-of-controls (read-i16 p))
             (controls (read-list number-of-controls read-control p))
             (number-of-ugens (read-i16 p))
             (ugens (read-list number-of-ugens read-ugen p)))
        (make-graphdef
	 name
         constants
         control-defaults
         controls
         ugens)))))

(define read-graphdef-file
  (lambda (nm)
    (let* ((p (open-file-input-port nm))
           (g (read-graphdef p)))
      (close-port p)
      g)))

(define trace
  (lambda (msg x)
    (display msg)
    (display x)
    (newline)
    x))
